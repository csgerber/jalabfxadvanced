package edu.uchicago.gerber;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.

http://www.programmableweb.com/
http://www.faroo.com/
http://www.faroo.com/hp/api/api.html
http://www.faroo.com/api?q=iphone&start=1&length=10&l=en&src=web&f=json
http://jsonviewer.stack.hu/
 */


import com.google.gson.Gson;
import edu.uchicago.gerber.nyt.NytSearchResults;
import edu.uchicago.gerber.yelp.HttpDownUtil;
import edu.uchicago.gerber.yelp.Yelp;
import edu.uchicago.gerber.yelp.YelpSearchResults;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import javafx.animation.Animation;
import javafx.animation.Interpolator;
import javafx.animation.RotateTransition;
import javafx.application.Platform;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import static javafx.concurrent.Worker.State.CANCELLED;
import static javafx.concurrent.Worker.State.FAILED;
import static javafx.concurrent.Worker.State.READY;
import static javafx.concurrent.Worker.State.RUNNING;
import static javafx.concurrent.Worker.State.SCHEDULED;
import static javafx.concurrent.Worker.State.SUCCEEDED;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.ProgressBarTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.effect.BoxBlur;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author ag
 */
public class TimesController implements Initializable {

    @FXML
    private TextField txtSearch;

    @FXML
    private Button btnGo;
    @FXML
    private ListView<String> lstView;
    @FXML
    private Label lblStatus;

    @FXML
    private Label lblTitle;

    YelpSearchResults yelpSearchResultLocal;

    @FXML
    private TableView<DownTask> table;

 

    public class GetReviewsTask extends Task<ObservableList<String>> {

   
        private String convertSpacesToPluses(String strOrig) {
            return strOrig.replace(" ", "+");
        }

        @Override
        protected ObservableList<String> call() throws Exception {

            ObservableList<String> sales = FXCollections.observableArrayList();
            updateMessage("Finding movies  . . .");
            String strUrl = "http://api.nytimes.com/svc/movies/v2/reviews/search.json?query=";
            String strQuery = strUrl + convertSpacesToPluses(txtSearch.getText()) + "&thousand-best=Y&api-key=6aac490155e332f996fc982556631835:14:69615440";
            NytSearchResults nytResult = new Gson().fromJson(getJSON(strQuery, 0), NytSearchResults.class);
            ArrayList<String> strResults = nytResult.getUrlValues();

            if (strResults == null || strResults.size() == 0) {
                updateMessage("No data found for that search term...try again");
            } else {
                updateMessage("movies data found");
            }

            //this will get returned and set to the lstView
            return FXCollections.observableArrayList(strResults);

        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

       // table = new TableView<DownTask>();
        TableColumn<TimesController.DownTask, String> statusCol = new TableColumn("Status");
        statusCol.setCellValueFactory(new PropertyValueFactory<TimesController.DownTask, String>(
                "message"));
        statusCol.setPrefWidth(100);

        TableColumn<TimesController.DownTask, Double> progressCol = new TableColumn("Progress");
        progressCol.setCellValueFactory(new PropertyValueFactory<TimesController.DownTask, Double>(
                "progress"));
        
        progressCol.setPrefWidth(125);

        //this is the most important call
        progressCol
                .setCellFactory(ProgressBarTableCell.<TimesController.DownTask>forTableColumn());

        TableColumn<TimesController.DownTask, String> fileCol = new TableColumn("File");
        fileCol.setCellValueFactory(new PropertyValueFactory<TimesController.DownTask, String>(
                "title"));
        fileCol.setPrefWidth(375);

        //add the cols
        table.getColumns().addAll(statusCol, progressCol, fileCol);

        btnGo.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                
                //you can instantiate a new Task each time if you want to "re-use" it, but once this intance is cancelled, you can't restart it again. 
                final Task<ObservableList<String>> getReviewsTask = new GetReviewsTask();

                table.getItems().clear();
                lblStatus.textProperty().bind(getReviewsTask.messageProperty());
                btnGo.disableProperty().bind(getReviewsTask.runningProperty());
                lstView.itemsProperty().bind(getReviewsTask.valueProperty());

                getReviewsTask.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
                    @Override
                    public void handle(WorkerStateEvent t) {
                        
                     ObservableList<String> observableList =   getReviewsTask.getValue();
                     if (observableList == null || observableList.size() == 0){
                         return;
                     }
                        
                     //add Tasks to the table
                        for (String str :  getReviewsTask.getValue()) {
                            table.getItems().add(new DownTask(str, "c:/dev"));
                        }

                        //fire up executor-service with limted number of threads
                        ExecutorService executor = Executors.newFixedThreadPool(3, new ThreadFactory() {
                            @Override
                            public Thread newThread(Runnable r) {
                                Thread t = new Thread(r);
                                t.setDaemon(true);
                                return t;
                            }
                        });

                        for (TimesController.DownTask pbarTask : table.getItems()) {
                            executor.execute(pbarTask);
                        }

                    }
                });


                new Thread(getReviewsTask).start();

            }
        });

    }

    class DownTask extends Task<Void> {

        private String pdfFrom, pdfTo;

        public DownTask(String pdfFrom, String pdfTo) {
            this.pdfFrom = pdfFrom;
            this.pdfTo = pdfTo;

        }

        @Override
        protected Void call() throws Exception {

            HttpDownUtil util = new HttpDownUtil();
            try {

                this.updateProgress(ProgressIndicator.INDETERMINATE_PROGRESS, 1);
                this.updateMessage("Downloading...");
           //     util.downloadFile(this.pdfFrom);

            //    InputStream inputStream = util.getInputStream();
                // opens an output stream to save into file
            //    FileOutputStream outputStream = new FileOutputStream(pdfTo + "/" + util.getFileName());
                this.updateTitle( this.pdfFrom);

           //     byte[] buffer = new byte[4096];
          //      int bytesRead = -1;
          //      long totalBytesRead = 0;
          //      int percentCompleted = 0;
          //      long fileSize = util.getContentLength();

//                while ((bytesRead = inputStream.read(buffer)) != -1) {
//                    outputStream.write(buffer, 0, bytesRead);
//                    totalBytesRead += bytesRead;
//                    percentCompleted = (int) (totalBytesRead * 100 / fileSize);
//                    updateProgress(percentCompleted, 100);
//                }

               
                int nRand = new Random().nextInt(200);
                int nC = 0;
                while (nC < 100){
                
                    Thread.sleep(nRand);
                    updateProgress(nC++, 100);
                }
                
                
                updateMessage("Complete");
             //   outputStream.close();
            //    util.disconnect();

            } catch (Exception ex) {
                ex.printStackTrace();
                this.cancel(true);
                table.getItems().remove(this);

            }

            return null;
        }

    }

    //this is a UI-thread-blocking operation and should be called in background thread
    public String getJSON(String url, int timeout) {
        try {
            URL u = new URL(url);
            HttpURLConnection c = (HttpURLConnection) u.openConnection();
            c.setRequestMethod("GET");
            c.setRequestProperty("Content-length", "0");
            c.setUseCaches(false);
            c.setAllowUserInteraction(false);
            c.setConnectTimeout(timeout);
            c.setReadTimeout(timeout);
            c.connect();
            int status = c.getResponseCode();

            switch (status) {
                case 200:
                case 201:
                    BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    br.close();
                    return sb.toString();
            }

        } catch (MalformedURLException ex) {
            // Logger.getLogger(DebugServer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            // Logger.getLogger(DebugServer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

}


