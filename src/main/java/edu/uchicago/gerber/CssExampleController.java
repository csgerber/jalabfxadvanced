/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.uchicago.gerber;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author ag
 */
public class CssExampleController implements Initializable {
    @FXML
    private TextField txtName;
    @FXML
    private TextField txtPass;
    @FXML
    private Button btnSign;
    @FXML
    private Label lblWelcome;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
 
    }    

    @FXML
    private void btnSignAction(ActionEvent event) {
        System.out.println( txtName.getText() + txtPass.getText());
       
    }
    
}
